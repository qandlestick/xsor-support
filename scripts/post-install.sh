#!/bin/bash
set -x

function platformize(){

#Linux OS detection#
 if hash lsb_release; then
   echo "Ubuntu server OS detected"
   export APP_HOME="/home/ubuntu"

 elif hash yum; then
  echo "Amazon Linux detected"
  export APP_HOME="/home/ec2-user"

 else
   echo "Unsupported release"
   exit 1

 fi
}


platformize

APP=sor

cp -r /tmp/sor.uploads/*.jar ${APP_HOME}/libs/${APP}.jar
cp -r /tmp/sor.uploads/*.sh ${APP_HOME}/bin/

