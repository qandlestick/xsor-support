#!/bin/bash

function platformize(){

#Linux OS detection#
 if hash lsb_release; then
   echo "Ubuntu server OS detected"
   export APP_HOME="/home/ubuntu"

 elif hash yum; then
  echo "Amazon Linux detected"
  export APP_HOME="/home/ec2-user"

 else
   echo "Unsupported release"
   exit 1

 fi
}


platformize

pid_file=${APP_HOME}/sor.pid

if [[ -e ${pid_file} ]]; then
	echo "Stopping sor..."
	sudo pkill -F ${pid_file}
	while [ -e /proc/$(<${pid_file}) ]; do sleep 1; done
else
	echo "Application is not running - all ok!"
fi
