@echo off
rem docker-compose -f ../docker/docker-compose.yaml up -d

docker stack deploy -c ../docker/docker-infrastructure.yaml xps-infra
docker stack deploy -c ../docker/docker-compose.yaml xps
