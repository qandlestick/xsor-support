#!/usr/bin/env bash

AWS_PASSWORD=$1

docker login --username AWS --password $AWS_PASSWORD 069737339215.dkr.ecr.us-east-2.amazonaws.com

docker swarm init
docker pull 069737339215.dkr.ecr.us-east-2.amazonaws.com/mainbloq/xsor-server:2.1.0-SNAPSHOT
docker pull 069737339215.dkr.ecr.us-east-2.amazonaws.com/mainbloq/xsor-gui:1 
