@echo off
set version="2.1.0-SNAPSHOT"
set namespace="069737339215.dkr.ecr.us-east-2.amazonaws.com/"

pushd ..
docker build -t %namespace%mainbloq/xsor-server:%version% --build-arg=%version% -f ./docker/xsor-base.dockerfile .
popd
