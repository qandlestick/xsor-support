#!/bin/bash
# io.swagger:mainbloq-bequant-client:1.0.0
mvn install:install-file -Dfile=sor-exchanges/libs/mainbloq-bequant-client-1.0.0.jar -DgroupId=io.mainbloq.bequant -DartifactId=mainbloq-bequant-client -Dversion=1.0.0 -Dpackaging=jar -DgeneratePom=true
mvn install:install-file -Dfile=sor-exchanges/libs/binance-api-client-1.0.6-MDVX-SNAPSHOT.jar -DgroupId=com.binance.api -DartifactId=binance-api-client -Dversion=1.0.6-MDVX-SNAPSHOT -Dpackaging=jar -DgeneratePom=true
mvn install:install-file -Dfile=sor-exchanges/libs/binance-client-1.0.9-MDVX-SNAPSHOT.jar -DgroupId=com.binance.sdk -DartifactId=binance-client -Dversion=1.0.9-MDVX-SNAPSHOT -Dpackaging=jar -DgeneratePom=true
mvn install:install-file -Dfile=sor-exchanges/libs/okex-java-sdk-api-1.0.4-MDVX-SNAPSHOT.jar -DgroupId=com.okcoin.commons -DartifactId=okex-java-sdk-api -Dversion=1.0.4-MDVX-SNAPSHOT -Dpackaging=jar -DgeneratePom=true
mvn install:install-file -Dfile=sor-exchanges/libs/huobi-client-2.0.3-SNAPSHOT.jar -DgroupId=com.huobi.sdk -DartifactId=huobi-client -Dversion=2.0.3-SNAPSHOT -Dpackaging=jar -DgeneratePom=true