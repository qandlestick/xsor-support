#!/bin/bash
set -x

function platformize() {

  #Linux OS detection#
  if hash lsb_release; then
    echo "Ubuntu server OS detected"
    export APP_HOME="/home/ubuntu"

  elif hash yum; then
    echo "Amazon Linux detected"
    export APP_HOME="/home/ec2-user"

  else
    echo "Unsupported release"
    exit 1

  fi
}

platformize

APP=sor

DEPLOY_PACKAGE=$(ls -t ${APP_HOME}/libs/sor*.jar | head -1)
if [[ -z ${DEPLOY_PACKAGE} ]]; then
  echo "Deployment package not found."
  exit 1
fi

echo "Deploy Package: ${DEPLOY_PACKAGE}"

${APP_HOME}/bin/xsor-stop.sh
cd ${APP_HOME}

echo "Starting ${APP}..."
nohup java -Xmx256M -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=9191 -jar ${DEPLOY_PACKAGE} >${APP_HOME}/logs/$APP.log 2>&1 &
exit_code=$?
pid=$!

if [[ ${exit_code} != 0 ]]; then
  echo "Error: Start failed! ${exit_code}"
fi
echo ${pid} >${APP_HOME}/$APP.pid
