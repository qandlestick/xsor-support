#!/bin/bash -x
export APP=chimera
cd ~
cp -p -u ~/libs/$APP.jar ~/libs/$APP.prev.jar
cp -p -u "`ls -tr ~/uploads/*.jar | tail -1`" ~/libs/$APP.jar
pkill -F ~/$APP.pid
if test -f ~/$APP.pid; then
  while [ -e /proc/$(<~/$APP.pid) ]; do sleep 5; pkill -9 -F ~/$APP.pid; done
fi

export JDEBUG=-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:9191
#export JFX=--module-path ~/libs/javafx-sdk-11.0.2/lib --add-modules javafx.controls,javafx.media
export JRUN=-Xmx1500m
export LIGHTRUN=-agentpath:/home/ec2-user/agent/lightrun_agent.so
export PP='--trade -spot -arbNegative --min-returns 0.004 -n 50 -N 250 -T 50 -E 0.5 -a'

nohup java $JDEBUG $JFX $JRUN $LIGHTRUN -cp ~/libs/$APP.jar io.mainbloq.chimera.Chimera $PP > ~/logs/$APP.log 2>&1 &
echo $! > ~/$APP.pid
