export APP=sor
cd ~
cp -u ~/libs/$APP.jar ~/libs/$APP.prev.jar
cp -u "`ls -tr ~/uploads/sor-*.jar | tail -1`" ~/libs/$APP.jar

pkill -F ~/$APP.pid
if test -f ~/$APP.pid; then
   while [ -e /proc/$(<~/$APP.pid) ]; do sleep 5; pkill -9 -F ~/$APP.pid; done
fi

export JPARAMS='-Xmx768M'
#export JDEBUG='-agentlib:jdwp=transport=dt_socket,server=y,suspend=y,address=8998'
#export LIGHTRUN='-agentpath:/home/ec2-user/agent/lightrun_agent.so'

nohup java $JPARAMS $JDEBUG $LIGHTRUN -jar ~/libs/$APP.jar > ~/logs/$APP.log 2>&1 &
echo $! > ~/$APP.pid

